<?php include 'header.html';?>

<?php
$page = isset($_GET['followers']) ? 'followers' : 'friends';

$emailContent = [
  'followers' => [
    'title' => 'A new friend have just followed you',
    'description' => 'Name (whoever has followed) has just followed you. Follow her back to join his/her food journey and see what they are sharing.',
  ],

  'friends' => [
    'title' => 'Join your friends to create a community of great foodies',
    'description' => 'Follow your friends and create your own community of foodies, sharing honest reviews and entertaining posts to help other know best of the best restaurants with you.',
  ]
];
// var_dump($_GET['followers']);
?>

    <!-- content -->
    <table
      class="box-section pb-2"
      width="100%"
      cellspacing="0"
      cellpadding="0"
    >
      <tbody>
        <tr>
          <td align="left">
            <div class="title"><?php echo $emailContent[$page]['title'];?></div>

            <p><?php echo $emailContent[$page]['description'];?></p>
          </td>
        </tr>
      </tbody>
    </table>

    <div class="d-none- d-sm-none d-md-table m-auto">
      <?php for($i = 0; $i <= 2; $i++): ?>
      <!-- displays on device greater than or equal to 768px -->
      <table
        class="box-section facebook"
        width="100%"
        cellspacing="0"
        cellpadding="0"
      >
        <tbody>
          <tr>
            <?php for($j = 0; $j <= 2; $j++): ?>
            <td align="center">
              <!-- box-friends template -->
              <div class="box-friends">
                <a href="#!">
                  <img src="email_assets/friend.png" class="rounded-circle avatar">
                  <div class="name"><strong>Avay Shrestha</strong></div>
                  <div class="detail">155 Posts, 4211 Followers</div>
                </a>
              </div>
            </td>
            <?php endfor;?>
          </tr>
        </tbody>
      </table>
      <?php endfor;?>
    </div>


    <?php 
    /*
      <div class="d-none- d-sm-table d-md-none m-auto">
        <?php for($i = 0; $i <= 3; $i++): ?>
        <!-- displays on device greater than or equal to 768px -->
        <table
          class="box-section"
          width="100%"
          cellspacing="0"
          cellpadding="0"
        >
          <tbody>
            <tr>
              <?php for($j = 0; $j <= 1; $j++): ?>
              <td align="center">
                <!-- box-friends template -->
                <div class="box-friends">
                  <a href="#!">
                    <img src="email_assets/friend.png" class="rounded-circle avatar">
                    <div class="name"><strong>Avay Shrestha</strong></div>
                    <div class="detail">155 Posts, 4211 Followers</div>
                  </a>
                </div>
              </td>
              <?php endfor;?>
            </tr>
          </tbody>
        </table>
        <?php endfor;?>
      </div>


      <div class="d-table d-sm-none d-md-none m-auto">
        <?php for($i = 0; $i <= 8; $i++): ?>
        <!-- displays on device greater than or equal to 768px -->
        <table
          class="box-section"
          width="100%"
          cellspacing="0"
          cellpadding="0"
        >
          <tbody>
            <tr>
              <?php for($j = 0; $j < 1; $j++): ?>
              <td align="center">
                <!-- box-friends template -->
                <div class="box-friends">
                  <a href="#!">
                    <img src="email_assets/friend.png" class="rounded-circle avatar">
                    <div class="name"><strong>Avay Shrestha</strong></div>
                    <div class="detail">155 Posts, 4211 Followers</div>
                  </a>
                </div>
              </td>
              <?php endfor;?>
            </tr>
          </tbody>
        </table>
        <?php endfor;?>
      </div>
    */ ?>



    <table
      class="box-section pt-3 pb-5"
      width="100%"
      cellspacing="0"
      cellpadding="0"
    >
      <tbody>
        <tr>
          <td align="center">
            <a href="#" class="btn">See 4+ more</a>
          </td>
        </tr>
      </tbody>
    </table>

<?php include 'footer.html';?>