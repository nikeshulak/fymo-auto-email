<?php include 'header.html';?>

<!-- <link rel="stylesheet" href="feed.css"> -->
<style>
  /* metronic */
  .m-portlet.m-portlet--tab {
      border: 1px solid #d7d7d7;
  }
  .m-widget3 .m-widget3__item .m-widget3__header {
      display: table;
      margin-bottom: 10px;
  }
  .m-widget3 .m-widget3__item .m-widget3__header .m-widget3__info {
      display: table-cell;
      width: 100%;
      padding-left: 1rem;
      padding-top: -0.57rem;
      font-size: 1rem;
      vertical-align: middle;
  }
  .m-portlet .m-portlet__body {
      padding: 20px;
  }

  .m-widget3 .m-widget3__item .m-widget3__header .m-widget3__user-img .m-widget3__img {
      border-radius: 50%;
      width: 40px;
  }

  .m-widget3 .m-widget3__item .m-widget3__header .m-widget3__info .m-widget3__time {
      font-size: .85rem;
  }
  .m-widget3 .m-widget3__item .m-widget3__header .m-widget3__info .m-widget3__username {
      font-size: 1rem;
      font-weight: 600;
  }






  /* feed */
  .relative,
  .m-widget3__info {
    position: relative;
  }

  .m-widget3 .m-widget3__item.post-item {
    margin-bottom: 0;
  }
  .m-widget3 .m-widget3__item .m-widget3__header {
    /* padding-right: 25px; */
  }
  .post-item .post-img img {
    max-width: 100%;
    width: 100%;
  }

  span.post-img-more {
    position: absolute;
    height: 100%;
    width: 100%;
    color: #fff;
    background: rgba(0, 0, 0, 0.3);
    font-size: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .post-gallery a {
    position: relative;
  }

  .post-count a {
    color: #6d6d6d;
    cursor: pointer;
    margin-right: 10px;
    font-weight: 500;
    text-decoration: none;
  }

  .post-count {
    padding: 10px 0;
  }

  .post-comments {
    border-top: 1px solid #ddd;
    width: 100%;
  }
  .post-comments .post-comment {
    display: table;
    padding-top: 10px;
  }

  /* magnific popup */
  .ajax-modal-img {
    max-width: 1000px;
    margin: 20px auto;
    background: #fff;
    padding: 0;
    line-height: 0;
  }
  .ajax-modal-img .post-img img {
    width: 100%;
    height: auto;
  }
  .post-img {
    height: 100%;
  }
  .post-img-caption {
    line-height: 1.5;
    padding: 15px 0;
  }

  /* for more than 1 image */
  .post-gallery-multiple a {
    display: inline-block;
    width: 50%;
    float: left;
  }
  .post-gallery-3 a {
    display: inline-block;
    width: 33.33%;
    float: left;
  }
  a:hover {
    text-decoration: none;
  }

  /* likes */
  .ajax-modal {
    background: #fff;
    max-width: 400px;
    margin: 0 auto;
    padding: 15px;
    position: relative;
  }
  .ajax-modal-head {
    border-bottom: 1px solid #ddd;
    margin-bottom: 20px;
  }
  .post-likes .m-widget3 .m-widget3__item .m-widget3__header .m-widget3__info {
    vertical-align: text-bottom;
  }

  .post-likes .m-widget3 .m-widget3__item .m-widget3__header {
    border-bottom: 1px solid #ddd;
    margin-bottom: 15px;
    padding-bottom: 15px;
  }

  .post-likes .m-widget3 .m-widget3__item .m-widget3__header:last-child {
    border-bottom: 0;
  }

  /* post reposted */
  .post-original .post-gallery-multiple img {
    width: 50%;
    float: left;
  }
  .post-original a {
    color: inherit;
  }
  .post-original {
    border: 1px solid #ddd;
    padding: 15px;
    margin-top: 20px;
    margin-bottom: 10px;
  }

  .post-item .m-widget3__header {
    margin-bottom: 10px;
  }
  .post-comment .m-widget3__info {
    padding-right: 25px;
  }
  .m-widget3 .m-widget3__item .m-widget3__header.post-comments {
    padding-right: 0;
  }

  .view-all-comments {
    margin-top: 15px;
  }
  .post-img img {
      max-width: 100%;
      height: 100%;
      object-fit: cover;
  }

  .edit-link {
      float: right;
  }
</style>

    <!-- content -->
    <table
      class="box-section pb-0"
      width="100%"
      cellspacing="0"
      cellpadding="0"
    >
      <tbody>
        <tr>
          <td align="left">
            <div class="title">Your friend has a new post</div>

            <p>Confused where to dine out? Get to know where your foodie friends are dining out and you might get the solution as you know FYMO has it all; from your favorite restaurants to the restaurants you have never visited. Explore the posts and know where your favorite cuisines are served. </p>
          </td>
        </tr>
      </tbody>
    </table>

    <!-- feed -->
    <?php
    $feedItems = [
      [
        "bannerImageCount" => 1,
        "bannerHeight" => "auto",
        "bannerImage" => [
          "email_assets/photo1.jpg"
        ]
      ],
      [
        "bannerImageCount" => 2,
        "bannerHeight" => "285.063px",
        "bannerImage" => [
          "email_assets/photo1.jpg",
          "email_assets/photo2.jpg"
        ]
      ],
      [
        "bannerImageCount" => 3,
        "bannerHeight" => "285.063px",//TODO => need to use equal height jquery
        "bannerImage" => [
          "email_assets/photo1.jpg",
          "email_assets/photo2.jpg",
          "email_assets/photo2.jpg"
        ]
      ]
    ];
    // print_r($feedItems);
    //TODO: need to use equal height jquery

    foreach ($feedItems as $key => $item):
      // echo $key;
    ?>
    <table
      class="box-section"
      width="100%"
      cellspacing="0"
      cellpadding="0"
    >
      <tbody>
        <tr>
          <td align="left">
            <div class="m-portlet m-portlet--tab">
              <div class="m-portlet__body">

                
                <div class="m-widget3 relative">
                  <div class="edit-link">
                    <img src="email_assets/ellipsis-h.png">
                  </div>
                  
                  <div class="m-widget3__item post-item hide-comment">

                    <div class="m-widget3__header">
                      
                      <div class="m-widget3__user-img">
                        <img class="m-widget3__img" src="email_assets/user1.jpg" alt="">
                      </div>
                      
                      <div class="m-widget3__info">
                        <a href="#!">
                            <span class="m-widget3__username">Melania Trump</span>
                        </a>
                        
                        <span>posted a picture with <a href="#!">John</a> &amp; <a href="#!">3 others</a> in </span>
                        <a href="#!"><i class="fa fa-map-marker-alt"></i> Karma Lounge</a>                        
                                
                        <br>
                        <span class="m-widget3__time">
                          <!-- 2 day ago -->
                          8 months ago
                        </span>
                      </div>
                    </div>
    
                    <div class="m-widget3__body">
                      <p>
                        Lorem ipsum dolor sit amet,consectetuer edipiscing elit,sed diam nonummy nibh euismod tinciduntut laoreet doloremagna aliquam erat volutpat.
                      </p>

                      <div class="post-img">

                        <div 
                          class="post-gallery clearfix <?php if($item["bannerImageCount"] !== 1) echo 'post-gallery-multiple'; ?>"
                        >
                          <?php foreach ($item["bannerImage"] as $keyBanner => $itemBanner): ?>
                          <a 
                            href="#!" 
                            class="<?php if($keyBanner >= 2) echo 'd-none';?>"
                            style="height: <?php echo $item['bannerHeight'];?>"
                          >
                            <?php if($item["bannerImageCount"] >= 3 && $keyBanner === 1): ?>
                            <span 
                            class="post-img-more"
                            >
                            +1
                            </span>
                            <?php endif;?>
                            <img src="<?php echo $itemBanner;?>">
                          </a>
                          <?php endforeach;?>
                        </div>

                      </div>
                      
                      <div class="post-count">
                        <a href="#!" class="post-likes-btn">2 Likes</a>
                        <a href="#!" class="post-comment-btn">3 Comments</a>
                        <a href="#!" class="post-shares-btn">2 Shares</a>
                      </div>

                    </div>
                  </div>
                </div>
                <!-- end .m-widget3 -->

              </div>
            </div>

          </td>
        </tr>
      </tbody>
    </table>
    <?php
    endforeach;
    ?>

    

    <table
      class="box-section pt-2 pb-5"
      width="100%"
      cellspacing="0"
      cellpadding="0"
    >
      <tbody>
        <tr>
          <td align="center">
            <a href="#!" class="btn">See 4+ more</a>
          </td>
        </tr>
      </tbody>
    </table>

<?php include 'footer.html';?>