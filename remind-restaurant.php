
    <div class="d-none- d-sm-none d-md-table m-auto">
      <?php for($i = 0; $i <= 2; $i++): ?>
      <!-- displays on device greater than or equal to 768px -->
      <table
        class="box-section facebook"
        width="100%"
        cellspacing="0"
        cellpadding="0"
      >
        <tbody>
          <tr>
            <?php for($j = 0; $j <= 2; $j++): ?>
            <td align="center">
              <!-- box-friends template -->
              <div class="box-friends">
                <a href="#!">
                  <figure>
                    <button class="badge badge-success">4.5</button>
                    <img src="email_assets/restaurant.png" class="rounded avatar">
                  </figure>
                  <div class="name"><strong>Phileo Coffee Shop</strong></div>
                  <div class="detail">Pradashani Marg, Bhirku...</div>
                </a>
              </div>
            </td>
            <?php endfor;?>
          </tr>
        </tbody>
      </table>
      <?php endfor;?>
    </div>


    <?php 
    /*
      <div class="d-none d-sm-table d-md-none m-auto">
        <?php for($i = 0; $i <= 3; $i++): ?>
        <!-- displays on device greater than or equal to 768px -->
        <table
          class="box-section"
          width="100%"
          cellspacing="0"
          cellpadding="0"
        >
          <tbody>
            <tr>
              <?php for($j = 0; $j <= 1; $j++): ?>
              <td align="center">
                <!-- box-friends template -->
                <div class="box-friends">
                  <figure>
                    <button class="badge badge-success">4.5</button>
                    <img src="email_assets/restaurant.png" class="rounded avatar">
                  </figure>
                  <div class="name"><strong>Phileo Coffee Shop</strong></div>
                  <div class="detail">Pradashani Marg, Bhirku...</div>
                </div>
              </td>
              <?php endfor;?>
            </tr>
          </tbody>
        </table>
        <?php endfor;?>
      </div>
    


      <div class="d-table d-sm-none d-md-none m-auto">
        <?php for($i = 0; $i <= 8; $i++): ?>
        <!-- displays on device greater than or equal to 768px -->
        <table
          class="box-section"
          width="100%"
          cellspacing="0"
          cellpadding="0"
        >
          <tbody>
            <tr>
              <?php for($j = 0; $j < 1; $j++): ?>
              <td align="center">
                <!-- box-friends template -->
                <div class="box-friends">
                  <figure>
                    <button class="badge badge-success">4.5</button>
                    <img src="email_assets/restaurant.png" class="rounded avatar">
                  </figure>
                  <div class="name"><strong>Phileo Coffee Shop</strong></div>
                  <div class="detail">Pradashani Marg, Bhirku...</div>
                </div>
              </td>
              <?php endfor;?>
            </tr>
          </tbody>
        </table>
        <?php endfor;?>
      </div>
    */ ?>