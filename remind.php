<?php include 'header.html';?>

    <!-- content -->
    <table
      class="box-section pb-2"
      width="100%"
      cellspacing="0"
      cellpadding="0"
    >
      <tbody>
        <tr>
          <td align="left">
            <div class="title">Treat Yourself with FYMO COIN </div>

            <p>You have 10 FC, do visit the listed restaurants in Spend FC section to see which restaurants you can eat with your FYMO Coins.  </p>
          </td>
        </tr>
      </tbody>
    </table>

    <?php include 'remind-restaurant.php';?>

    <table
      class="box-section pt-2 pb-5"
      width="100%"
      cellspacing="0"
      cellpadding="0"
    >
      <tbody>
        <tr>
          <td align="center">
            <a href="#" class="btn">See 4+ more</a>
          </td>
        </tr>
      </tbody>
    </table>

<?php include 'footer.html';?>